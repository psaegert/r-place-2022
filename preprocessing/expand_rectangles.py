import multiprocessing as mp
import pandas as pd

def expand_all_rectangles(file_paths):
    print('Expanding all rectangles')
    print('Using {} cores'.format(mp.cpu_count()))
    pool = mp.Pool(mp.cpu_count())
    expanded_rows = pool.map(expand_rectangles, file_paths)
    pool.close()
    pool.join()
    return expanded_rows

def expand_rectangles(file):
    df = pd.read_csv(file)

    # Create an empty dataframe to store the new rows
    new_df = pd.DataFrame()

    # Create a list of rows to drop later
    rows_to_drop = []
    
    # Iterate through the rows of the dataframe
    for i, row in df.iterrows():
        # If the row describes a rectangle, duplicate the row for each coordinate in the rectangle
        if row['coordinate'].count(',') == 3:
            # Split the coordinate column into two columns (x and y)
            x1, y1, x2, y2 = row['coordinate'].split(',')

            # Iterate through the coordinates in the rectangle
            for x in range(int(x1), int(x2) + 1):
                for y in range(int(y1), int(y2) + 1):
                    # Create a new row with the coordinates (x,y)
                    new_row = row.copy()
                    new_row['coordinate'] = '{},{}'.format(x, y)

                    # Append the new row to the new dataframe
                    new_df = new_df.append(new_row)
            
            # Remember to drop the original row
            rows_to_drop.append(i)
        
    
    # Drop the original rows
    df = df.drop(rows_to_drop)

    # Concatenate the new rows to the original dataframe
    df = pd.concat([df, new_df])

    # Sort the dataframe by timestamp, since appending the new rows will not preserve the order of the rows
    df = df.sort_values(by='timestamp')

    # Reset the index of the dataframe
    df = df.reset_index(drop=True)

    # Overwrite the original file with the new dataframe (assuming backups exist somwhere else)
    # If the new dataframe is empty, do not update the file
    if len(new_df) > 0:
        df.to_csv(file, index=False)
    
    return len(new_df)

def test_multiprocessing(file_paths):
    pool = mp.Pool(mp.cpu_count())
    expanded_rows = pool.map(test_expand, file_paths)
    pool.close()
    pool.join()
    return expanded_rows

def test_expand(file):
    df = pd.read_csv(file)
    return len(df)