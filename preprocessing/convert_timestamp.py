import multiprocessing as mp
import pandas as pd

def convert_all_timestamps(file_paths):
    print('Converting all timestamps...')
    print('Using {} cores'.format(mp.cpu_count()))
    pool = mp.Pool(mp.cpu_count())
    sucesses = pool.map(convert_timestamps, file_paths)
    pool.close()
    pool.join()
    return sucesses

def convert_timestamps(file):
    df = pd.read_csv(file, parse_dates=['timestamp'])
    
    # Convert the timestamp column into a unix timestamp column
    df['unix'] = df['timestamp'].map(lambda x: int(1000 * x.timestamp()))
    
    del df['timestamp']

    # Overwrite the original file with the new dataframe (assuming backups exist somwhere else)
    df.to_csv(file, index=False)

    return True