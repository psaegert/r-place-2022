import multiprocessing as mp
import pandas as pd

def split_all_coordinates(file_paths):
    print('Splitting all coordinates')
    print('Using {} cores'.format(mp.cpu_count()))
    pool = mp.Pool(mp.cpu_count())
    sucesses = pool.map(split_coordinates, file_paths)
    pool.close()
    pool.join()
    return sucesses

def split_coordinates(file):
    df = pd.read_csv(file)
    
    df['x'] = df['coordinate'].map(lambda x: x.split(',')[0])
    df['y'] = df['coordinate'].map(lambda x: x.split(',')[1])
        

    # Remove the original column from the dataset
    del df['coordinate']

    # Overwrite the original file with the new dataframe (assuming backups exist somwhere else)
    df.to_csv(file, index=False)

    return True