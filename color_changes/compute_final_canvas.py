# This script is used to compute the final canvas by iterating through the dataframe in reverse order.

import pandas as pd
import numpy as np
from tqdm import tqdm

print("Loading data...")
df = pd.read_csv('../data.csv')
print("Data loaded.")

# Find out at which point only white pixels were placed by finding the last index at which the color of the pixel placed was different from the last color placed.
final_canvas_index = np.where(np.diff(np.array(df['color'])) != 0)[0][-1]
print(f'Last index before whiteout: {final_canvas_index} ({final_canvas_index/len(df)*100:.2f}%)')

# Compute the actual final distribution of colors by backtracking the dataframe
# Create an empty array of the same size as the canvas (2000, 2000). This will be filled with the colors.
final_canvas = np.full((2000, 2000), -1)

print("Computing final canvas...")
# Iterate through the df in reverse using iterrows()
for i, row in tqdm(df[final_canvas_index::-1].iterrows(), total=len(df)):
    # If the pixel is not already filled, fill it with the color of the current row.
    if final_canvas[row['x'], row['y']] == -1:
        final_canvas[row['x'], row['y']] = row['color']

# Save the final canvas in a npy file
print("Saving final canvas...")
np.save('./color_changes_final_canvas.npy', final_canvas)